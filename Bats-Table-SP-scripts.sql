CREATE DATABASE  IF NOT EXISTS `recruitment-db-prod` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `recruitment-db-prod`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: adm-db.cado18fnhq3r.us-east-1.rds.amazonaws.com    Database: recruitment-db-prod
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Table structure for table `candidate_slot_schedule`
--

DROP TABLE IF EXISTS `candidate_slot_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `candidate_slot_schedule` (
  `slot_id` int(11) NOT NULL,
  `candidate_reg_id` varchar(250) NOT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`candidate_reg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `candidates_master`
--

DROP TABLE IF EXISTS `candidates_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `candidates_master` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `registration_no` varchar(40) NOT NULL,
  `status` varchar(25) NOT NULL DEFAULT 'ON_SUB',
  `full_name` varchar(100) NOT NULL,
  `first_apisero_interview` varchar(10) NOT NULL,
  `email_id` varchar(64) NOT NULL,
  `mobile_no` varchar(25) NOT NULL,
  `current_location` varchar(150) NOT NULL,
  `aadhar_no` varchar(25) NOT NULL,
  `preferred_location` varchar(150) NOT NULL,
  `address` varchar(512) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `primary_skills` varchar(64) DEFAULT NULL,
  `current_employer` varchar(100) DEFAULT NULL,
  `experience` double DEFAULT NULL,
  `notice_period` varchar(25) NOT NULL,
  `qualification` varchar(100) NOT NULL,
  `stream` varchar(100) NOT NULL,
  `percentage` varchar(10) NOT NULL,
  `college` varchar(150) NOT NULL,
  `university` varchar(100) NOT NULL,
  `year_of_completion` varchar(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `resume_location` varchar(512) NOT NULL,
  `source` varchar(512) NOT NULL DEFAULT 'LANDINGI',
  `IS_LOCKED` tinyint(1) DEFAULT '0',
  `LOCKED_BY` varchar(128) DEFAULT NULL,
  `referral_email` varchar(128) NOT NULL DEFAULT 'NON-REFERRAL',
  `news_source` varchar(128) DEFAULT NULL,
  `landingi_url` varchar(128) DEFAULT NULL,
  `token` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `registration_no` (`registration_no`)
) ENGINE=InnoDB AUTO_INCREMENT=49151 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `communication_channel`
--

DROP TABLE IF EXISTS `communication_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `communication_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` tinyint(1) NOT NULL DEFAULT '1',
  `whatsapp` tinyint(1) NOT NULL DEFAULT '0',
  `sms` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `interview_records`
--

DROP TABLE IF EXISTS `interview_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `interview_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_no` varchar(40) NOT NULL,
  `Interviewer` varchar(128) NOT NULL,
  `feedback` varchar(1024) NOT NULL,
  `result` varchar(30) NOT NULL,
  `current_round` varchar(30) NOT NULL,
  `next_round` varchar(30) NOT NULL,
  `updated_by` varchar(128) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2825 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `interview_slots`
--

DROP TABLE IF EXISTS `interview_slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `interview_slots` (
  `slot_id` int(11) NOT NULL AUTO_INCREMENT,
  `slot_date` date DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `zoom_meeting_id` varchar(45) NOT NULL,
  `zoom_meeting_password` varchar(45) DEFAULT NULL,
  `zoom_meeting_url` varchar(250) DEFAULT NULL,
  `proctor_email_id` varchar(100) DEFAULT NULL,
  `slot_availability` int(11) DEFAULT '30',
  PRIMARY KEY (`slot_id`,`zoom_meeting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prioritization`
--

DROP TABLE IF EXISTS `prioritization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prioritization` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `criteria` varchar(50) NOT NULL,
  `particulars` varchar(50) NOT NULL,
  `score` int(11) NOT NULL,
  `attribute1` varchar(50) DEFAULT NULL,
  `attribute2` varchar(50) DEFAULT NULL,
  `attribute3` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `round_names`
--

DROP TABLE IF EXISTS `round_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `round_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `round_name` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scores`
--

DROP TABLE IF EXISTS `scores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_no` varchar(40) NOT NULL,
  `algorithm_score` int(11) NOT NULL,
  `manual_score` int(11) NOT NULL,
  `oquiz_score` int(11) DEFAULT NULL,
  `quiz_scheduled` bit(1) DEFAULT b'0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `registration_no` (`registration_no`),
  UNIQUE KEY `registration_no_2` (`registration_no`)
) ENGINE=InnoDB AUTO_INCREMENT=30460 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'recruitment-db-prod'
--
/*!50003 DROP PROCEDURE IF EXISTS `CreateSlot` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `CreateSlot`(
	IN proctor_email_id varchar(50),
	IN slot_date datetime,
	IN start_time datetime,
	IN end_time datetime,
	IN zoom_meeting_url varchar(250),
	IN zoom_meeting_id varchar(50),
	IN zoom_meeting_password varchar(50),
	OUT status INT
)
BEGIN
	insert into interview_slots  (
						proctor_email_id,
						slot_date ,
						start_time ,
						end_time ,
						zoom_meeting_url ,
						zoom_meeting_id ,
						zoom_meeting_password )  
				VALUES(
						proctor_email_id,
						slot_date,
						start_time,
						end_time,
						zoom_meeting_url,
						zoom_meeting_id,
						zoom_meeting_password);

Set status = 1;
select STATUS;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_candidates_dup_records` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `delete_candidates_dup_records`()
BEGIN
DECLARE i INT;
SELECT COUNT(*) INTO @row_count FROM `candidates_duplicate_records_list`;
SELECT @row_count;
SET i= 1;

WHILE(i <= @row_count) DO

SET @email=(SELECT email_id FROM `candidates_duplicate_records_list` WHERE id=i);
SET @created_at=(SELECT created_at FROM `candidates_duplicate_records_list` WHERE id=i);
SET @aadhar_no =(SELECT aadhar_no FROM `candidates_duplicate_records_list` WHERE id=i);
set @full_name=(select full_name from candidates_duplicate_records_list where id=i);
SET @date_of_birth=(SELECT date_of_birth FROM candidates_duplicate_records_list WHERE id=i);
SET @mobile_no=(SELECT mobile_no FROM candidates_duplicate_records_list WHERE id=i);

-- set @registration_no=(select registration_no from candidate_unique where id=i);

SET i=i+1;

delete from candidates_duplicate where 	email_id=@email 
					AND aadhar_no=@aadhar_no  
					and full_name=@full_name
					and date_of_birth=@date_of_birth
					and mobile_no=@mobile_no
					and created_at < @created_at;



END WHILE;


	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetSlotDate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `GetSlotDate`()
BEGIN
	DECLARE slot_date_available date;
	SELECT DISTINCT(slot_date)  from interview_slots where slot_date>=CAST(CURRENT_TIMESTAMP() AS DATE) AND slot_availability>=1 order by slot_date ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetSlotTime` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `GetSlotTime`(
	IN slot_date_selected date
)
BEGIN
	SELECT DISTINCT start_time,end_time from interview_slots where start_time> CONVERT_TZ(CURRENT_TIMESTAMP(),'+00:00','+05:30') AND slot_date = slot_date_selected AND slot_availability>=1 order by start_time ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SendTestLink` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `SendTestLink`(
	IN sent_mail_date datetime
)
BEGIN
DROP TABLE if exists temp_email;
CREATE TEMPORARY  TABLE temp_email as (SELECT  c.email_id,c.registration_no ,c.status , c.full_name, c.mobile_no, s.algorithm_score
from candidates_master as c INNER JOIN scores as s on c.registration_no=s.registration_no
where s.quiz_scheduled=0  AND (c.status= 'OQUIZ_WAIT' or c.status='MANUAL_PRI') AND s.algorithm_score >= 40   AND s.created_at >=sent_mail_date AND s.created_at<=CONVERT_TZ(CURRENT_TIMESTAMP(),'+00:00','+05:30') order by algorithm_score desc LIMIT 2000);
SET SQL_SAFE_UPDATES=0;
UPDATE scores set quiz_scheduled=1, updated_at = CONVERT_TZ(CURRENT_TIMESTAMP(),'+00:00','+05:30') where registration_no in (SELECT registration_no FROM temp_email);
SET SQL_SAFE_UPDATES=1;
select * from temp_email;
END
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateToken` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `UpdateToken`(
IN c_token VARCHAR(8),
IN c_reg_id VARCHAR(50)
)
BEGIN
DECLARE c_email_id VARCHAR(50) DEFAULT NULL;
DECLARE c_full_name VARCHAR(50) DEFAULT NULL;
DECLARE c_mobile_no VARCHAR(50) DEFAULT NULL;
DECLARE c_status VARCHAR(50) DEFAULT NULL;
DECLARE c_reg_no VARCHAR(50) DEFAULT NULL;

SELECT 
    registration_no
INTO c_reg_no FROM
    `scores`
WHERE
    registration_no = c_reg_id
        AND quiz_scheduled = 1
LIMIT 1;
IF c_reg_no is not NULL THEN
SELECT  email_id,full_name,mobile_no,status into c_email_id,c_full_name,c_mobile_no,c_status  from `candidates_master` WHERE registration_no=c_reg_id LIMIT 1;
IF c_email_id is NOT NULL THEN
UPDATE `candidates_master` SET token=c_token WHERE registration_no=c_reg_id;
ELSE
SET c_email_id=NULL,c_full_name=NULL,c_mobile_no=NULL,c_status=NULL;
END IF;
ELSE
SET c_email_id=NULL,c_full_name=NULL,c_mobile_no=NULL,c_status=NULL;
END IF;
SELECT c_reg_id as registration_no,c_email_id AS email_id,c_full_name as full_name,c_mobile_no as mobile_no,c_status as status;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_candidates_duplicate_records` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `update_candidates_duplicate_records`()
BEGIN
DECLARE i INT;
SELECT COUNT(*) INTO @row_count FROM `candidates_duplicate_records_list`;
SELECT @row_count;
SET i= 1;

WHILE(i <= @row_count) DO

SET @email=(SELECT email_id FROM `candidates_duplicate_records_list` WHERE id=i);
SET @created_at=(SELECT created_at FROM `candidates_duplicate_records_list` WHERE id=i);
SET @aadhar_no =(SELECT aadhar_no FROM `candidates_duplicate_records_list` WHERE id=i);
SET @full_name=(SELECT full_name FROM candidates_duplicate_records_list WHERE id=i);
SET @date_of_birth=(SELECT date_of_birth FROM candidates_duplicate_records_list WHERE id=i);
SET @mobile_no=(SELECT mobile_no FROM candidates_duplicate_records_list WHERE id=i);

-- set @registration_no=(select registration_no from candidate_unique where id=i);



UPDATE `candidates_master` 
SET `status`="DUPLICATE"
					WHERE 	email_id=@email 
					AND aadhar_no=@aadhar_no  
					AND full_name=@full_name
					AND date_of_birth=@date_of_birth
					AND mobile_no=@mobile_no
					AND created_at < @created_at;
					
SET i=i+1;



END WHILE;


	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ValidateToken` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `ValidateToken`(
IN c_token VARCHAR(50),
IN c_reg_id VARCHAR(50)
)
BEGIN
DECLARE c_status INT DEFAULT 0;
DECLARE c_email VARCHAR(50) DEFAULT NULL;
SELECT email_id into c_email from `candidates_master` WHERE registration_no=c_reg_id and token=c_token;
IF c_email is NOT NULL THEN
SET c_status=1;
ELSE
SET c_status=0;
END IF;
SELECT c_status as STATUS;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-27  0:54:07


CREATE DEFINER=`admin`@`%` PROCEDURE `CreateSlot`(
	IN proctor_email_id varchar(50),
	IN slot_date datetime,
	IN start_time datetime,
	IN end_time datetime,
	IN zoom_meeting_url varchar(250),
	IN zoom_meeting_id varchar(50),
	IN zoom_meeting_password varchar(50),
    IN slot_availability int(50)
)
BEGIN
	insert into interview_slots  (
						proctor_email_id,
						slot_date ,
						start_time ,
						end_time ,
						zoom_meeting_url ,
						zoom_meeting_id ,
						zoom_meeting_password,
                        slot_availability)  
				VALUES(
						proctor_email_id,
						slot_date,
						start_time,
						end_time,
						zoom_meeting_url,
						zoom_meeting_id,
						zoom_meeting_password,
                        slot_availability);


END